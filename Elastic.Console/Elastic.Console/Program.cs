﻿using System.Diagnostics;
using Elastic.Console.Tools;
using Newtonsoft.Json;
using PlainElastic.Net;
using System;

namespace Elastic.Console
{
    class Program
    {
        const int SaveCount = 10;

        //TEST SONUÇLARI: 1000 insert 1000 ms te
        static void Main(string[] args)
        {
            System.Console.WriteLine("ElasticSearch Client Started");
            var krono = new Stopwatch();
            krono.Start();
            var myAccess = new ElasticSearchDataAccess();
            myAccess.SaveAllData();


            krono.Stop();
            System.Console.WriteLine("Elapsed time " + krono.Elapsed.TotalSeconds + " seconds");
            System.Console.WriteLine("ElasticSearch Client Ended");

            System.Console.Read();
        }

      



    }
}
