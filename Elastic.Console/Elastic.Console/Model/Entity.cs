using System.Collections.Generic;

namespace Elastic.Console
{
    public class Entity
    {
        public EntityType Type { get; set; }
        public List<Attribute> Attributes { get; set; }
    }
}