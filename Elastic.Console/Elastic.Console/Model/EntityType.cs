﻿using System.Collections.Generic;

namespace Elastic.Console
{
    public class EntityType
    {
        public string Name { get; set; }
        public List<AttributeType> AttributeTypes { get; set; }
    }
}