namespace Elastic.Console
{
    public class Attribute
    {
        public AttributeType AttributeType { get; set; }
        public string Value { get; set; }
    }
}