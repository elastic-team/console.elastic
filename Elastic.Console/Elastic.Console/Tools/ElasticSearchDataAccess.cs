﻿using System;
using System.Linq;
using Newtonsoft.Json;
using PlainElastic.Net;
using PlainElastic.Net.Queries;
using PlainElastic.Net.Serialization;

namespace Elastic.Console.Tools
{
    class ElasticSearchDataAccess
    {
        const string ElasticSearchServerUrl = "http://localhost:9200";

        public bool SaveAllData()
        {
            var myGen = new DataGenerator();
            var entityList = myGen.CreateEntityList();
            foreach (var myEnt in entityList)
            {
                SaveItemToElastic(myEnt);
            }

            return false;

        }

        private static bool SaveItemToElastic(Entity myEnt)
        {

            const string index = "elasticentity";
            const string type = "entity";
            var uniqueItem = Guid.NewGuid().ToString();
            var connection = new ElasticConnection();
            var command = String.Format("{0}/{1}/{2}/{3}", ElasticSearchServerUrl, index, type, uniqueItem);

            var jsonData = JsonConvert.SerializeObject(myEnt);
            var response = connection.Put(command, jsonData);

            return true;

        }


        //https://gist.github.com/Yegoroff/7679120
        //https://github.com/Yegoroff/PlainElastic.Net/blob/master/src/Samples/PlainSample/PlainSample.cs
        //install-package newtonsoft.json -version 4.0.7
        private static bool GetItemAdvancedFromElastic()
        {

            //string query = new QueryBuilder<user>()
            //             .Filter(f => f
            //                 .Term(t => t.Field(myObj => myObj.Type.Name).Value("John Doe))
            //              )
            //             .Size(1)
            //             .BuildBeautified();

            //var connection = new ElasticConnection("localhost", 9200);
            //var serializer = new JsonNetSerializer();

            //string result = connection.Post(Commands.Search("aom", "entity"), query);
            //var foundUser = serializer.ToSearchResult<EntityType>(result).Documents.First();


            return true;

        }

        private static bool GetItemBasicFromElastic()
        {
            const string commonUrl = "http://localhost:9200";
            const string index = "aom";
            const string type = "entity";
            var uniqueItem = "6f82f546-bca1-4047-805e-97bb71d4fd3c";
            var connection = new ElasticConnection();
            var command = String.Format("{0}/{1}/{2}/{3}", commonUrl, index, type, uniqueItem);
            var jsonData = "";
            var response = connection.Get(command, jsonData).Result;

            System.Console.WriteLine("Founded Type:" + response);
            return true;

        }


        private static bool GetItemComplexFromElastic()
        {
            string query = new QueryBuilder<Entity>()
                         .Filter(f => f
                             .Term(t => t.Field(myObj => myObj.Attributes[0].AttributeType.Name).Value("4154f"))
                          )
                         .Size(1)
                         .BuildBeautified();

            var connection = new ElasticConnection("localhost", 9200);
            var serializer = new JsonNetSerializer();

            string result = connection.Post(Commands.Search("aom", "entity"), query);
            var foundUser = serializer.ToSearchResult<Entity>(result).Documents.First();

            return true;

        }
    }
}
