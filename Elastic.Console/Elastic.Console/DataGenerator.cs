﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elastic.Console
{
    public class DataGenerator
    {
        const decimal EntityCount = 10000;
        const decimal SingleEntityAttributeCount = 100;

        public bool CreateData()
        {
            var krono = new Stopwatch();
            krono.Start();
            var sonuc = CreateEntityList();
            System.Console.WriteLine(string.Format("Data Created in {0} miliseconds", krono.Elapsed.TotalMilliseconds));
            if (sonuc.Count > 0)
                return true;
            return false;
        }

        public  List<Entity> CreateEntityList()
        {
            var mySonuc = new List<Entity>();

            for (int i = 0; i < EntityCount; i++)
            {
                var myEntity = CreateEntity();
                mySonuc.Add(myEntity);
            }
            return mySonuc;
        }

        private Entity CreateEntity()
        {
            var myEntity = new Entity {Type =new EntityType{Name=GenerateRandomString(5)}, Attributes = new List<Attribute>()};
            for (int i = 0; i < SingleEntityAttributeCount; i++)
            {
                myEntity.Attributes.Add(new Attribute
                {
                    Value = GenerateRandomString(5),
                    AttributeType = new AttributeType { Name = GenerateRandomString(5) }
                });
            }
            return myEntity;
        }

        public string GenerateRandomString(int stringLength)
        {
            var rnd = new Random();
            Guid guid;
            var randomString = string.Empty;

            int numberOfGuidsRequired = (int)Math.Ceiling((double)stringLength / 32d);
            for (int i = 0; i < numberOfGuidsRequired; i++)
            {
                guid = Guid.NewGuid();
                randomString += guid.ToString().Replace("-", "");
            }

            return randomString.Substring(0, stringLength);
        }
    }
}
